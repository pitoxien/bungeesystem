package de.claycode.bungeesystem.listener;

import de.claycode.bungeesystem.sql.NotifyStats;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ConnectionListener implements Listener {

    @EventHandler
    public void onConnect(ServerConnectEvent event) {
        final ProxiedPlayer player = event.getPlayer();

        if (player.hasPermission("bungee.notify")) {
            if(!NotifyStats.existsPlayer(player.getUniqueId().toString())) {
                NotifyStats.createPlayer(player.getUniqueId().toString());
            }
        }
    }
}
