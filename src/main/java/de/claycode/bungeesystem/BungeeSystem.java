package de.claycode.bungeesystem;

import de.claycode.bungeesystem.commands.*;
import de.claycode.bungeesystem.listener.ConnectionListener;
import de.claycode.bungeesystem.sql.SQL;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeSystem extends Plugin {

    public static BungeeSystem instance;
    public static SQL sql;

    @Override
    public void onEnable() {
        this.instance = this;

        registerSQL();
        register();
    }

    private void registerSQL() {
        this.sql = new SQL("134.255.217.76", "ProxySystem", "ProxySystem", "050405");
        this.sql.update("CREATE TABLE IF NOT EXISTS NOTIFY(UUID VARCHAR(100), SETTING VARCHAR(100));");
    }

    private void register() {
        getProxy().getPluginManager().registerListener(this, new ConnectionListener());

        getProxy().getPluginManager().registerCommand(this, new TeamSpeakCommand("ts"));
        getProxy().getPluginManager().registerCommand(this, new TeamSpeakCommand("teamspeak"));
        getProxy().getPluginManager().registerCommand(this, new NotifyCommand("notify"));
        getProxy().getPluginManager().registerCommand(this, new TeamChatCommand("tc"));
        getProxy().getPluginManager().registerCommand(this, new TeamChatCommand("teamchat"));
        getProxy().getPluginManager().registerCommand(this, new ReportCommand("report"));
        getProxy().getPluginManager().registerCommand(this, new BroadCastCommand("bc"));
        getProxy().getPluginManager().registerCommand(this, new BroadCastCommand("broadcast"));
        getProxy().getPluginManager().registerCommand(this, new JumpToCommand("jumpto"));
        getProxy().getPluginManager().registerCommand(this, new InfoCommand("info"));
        getProxy().getPluginManager().registerCommand(this, new WartungsCommand("wartungen"));
        getProxy().getPluginManager().registerCommand(this, new PingCommand("ping"));
    }
}
