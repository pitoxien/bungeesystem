package de.claycode.bungeesystem.sql;

import java.sql.*;

public class SQL {

    private String host;
    private String database;
    private String user;
    private String password;

    private Connection connection;

    public SQL(String host, String database, String user, String password) {
        this.host = host;
        this.database = database;
        this.user = user;
        this.password = password;

        onConnect();
    }

    private void onConnect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database + "?autoReconnect=true", user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void onDisconnect() {
        if(connection != null){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void update(String update) {
        try {
            final Statement statement = connection.createStatement();
            statement.executeUpdate(update);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet query(String query) {
        ResultSet resultSet = null;

        try {
            final Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }
}
