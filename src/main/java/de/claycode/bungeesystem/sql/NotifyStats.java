package de.claycode.bungeesystem.sql;

import de.claycode.bungeesystem.BungeeSystem;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NotifyStats {

    public static boolean existsPlayer(String uuid) {

        try {

            final ResultSet resultSet = BungeeSystem.sql.query("SELECT * FROM NOTIFY WHERE UUID= '" + uuid + "'");

            if (resultSet.next()) {
                return resultSet.getString("UUID") != null;
            }

            return false;

        }catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void createPlayer(String uuid) {
        if (!(existsPlayer(uuid))) {
            BungeeSystem.sql.update("INSERT INTO NOTIFY(UUID, SETTING) VALUES ('" + uuid + "', 'true');");
        }
    }

    public static void setNotify(String uuid, boolean setting) {
        if (existsPlayer(uuid)) {
            BungeeSystem.sql.update("UPDATE NOTIFY SET SETTING= '" + setting + "' WHERE UUID= '" + uuid + "';");
        }else{
            createPlayer(uuid);
        }
    }

    public static boolean getNotify(String uuid) {
        boolean setting = false;

        if (existsPlayer(uuid)) {
            try {
                final ResultSet resultSet = BungeeSystem.sql.query("SELECT * FROM NOTIFY WHERE UUID= '" + uuid + "'");

                if((!resultSet.next()) || (Boolean.valueOf(resultSet.getBoolean("SETTING")) == null));

                setting = resultSet.getBoolean("SETTING");
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            createPlayer(uuid);
        }

        return setting;
    }
}
