package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class JumpToCommand extends Command {

    public JumpToCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if(player.hasPermission("bungee.jumpto")) {
                if(args.length == 1) {
                    final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);

                    if(target != null) {
                        if(target != player) {
                            player.connect(target.getServer().getInfo());

                            player.sendMessage(Data.prefix + "Du bist dem Spieler §c" + target.getName() + " §7nachgesprungen.");
                            player.sendMessage(Data.prefix + " §8➥ §a" + target.getServer().getInfo().getName());
                        }else{
                            player.sendMessage(Data.prefix + "Du kannst §cnicht §7zu dir selber springen.");
                        }
                    }else{
                        player.sendMessage(Data.prefix + "Der Spieler ist §cnicht §7Online.");
                    }
                }else{
                    player.sendMessage(Data.prefix + "Versuche /§6Jumpto §8(§aSpieler§8)");
                }
            }else{
                player.sendMessage(Data.noPerm);
            }
        }
    }
}
