package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BroadCastCommand extends Command {

    public BroadCastCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission("bungee.broadcast")) {
                if(args.length == 0) {
                    player.sendMessage(Data.prefix + "Versuche /§6BroadCast §8(§aNachricht§8)");
                }else if(args.length >= 0) {
                    String message = "";

                    for (int i = 0; i < args.length; i++) {
                        message = message + " " + args[i];
                    }

                    ProxyServer.getInstance().broadcast("§8│» §e§lPitoxien §8► §8§m-----------------------------------");
                    ProxyServer.getInstance().broadcast("§8│» §e§lPitoxien §8►");
                    ProxyServer.getInstance().broadcast("§8│» §e§lPitoxien §8►§7" + message.replace("&", "§") + "");
                    ProxyServer.getInstance().broadcast("§8│» §e§lPitoxien §8►");
                    ProxyServer.getInstance().broadcast("§8│» §e§lPitoxien §8► §8§m-----------------------------------");
                }
            }else{
                player.sendMessage(Data.noPerm);
            }
        }
    }
}
