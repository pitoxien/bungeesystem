package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.sql.NotifyStats;
import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamChatCommand extends Command {

    public TeamChatCommand(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission("bungee.teamchat")) {
                if (args.length == 0) {
                    player.sendMessage(Data.prefix + "Versuche /§6TeamChat §8(§6Nachricht§8)");
                }else if (args.length >= 0) {
                    if (NotifyStats.getNotify(player.getUniqueId().toString()) == true) {
                        String message = "";

                        for (int i = 0; i < args.length; i++) {
                            message = message + " " + args[i];
                        }

                        for (ProxiedPlayer players : ProxyServer.getInstance().getPlayers())

                            if (players.hasPermission("bungee.teamchat")) {
                                if (NotifyStats.getNotify(players.getUniqueId().toString()) == true) {
                                    players.sendMessage("§8│» §e§lPitoxien §8►§7" + message);
                                }
                            }
                    }else{
                        player.sendMessage(Data.prefix + "Du bist §cnicht §7eingeloggt.");
                    }
                }
            }else{
                player.sendMessage(Data.noPerm);
            }
        }

    }
}
