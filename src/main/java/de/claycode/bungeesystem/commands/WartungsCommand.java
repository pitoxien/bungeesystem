package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.storage.Data;
import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.CloudProxy;
import de.dytanic.cloudnet.bridge.CloudServer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class WartungsCommand extends Command {

    public WartungsCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission("bungee.wartungen")) {
                if(args.length == 0) {
                    if (CloudProxy.getInstance().getProxyGroup().getProxyConfig().isMaintenance() == true) {
                        CloudProxy.getInstance().getProxyGroup().getProxyConfig().setMaintenance(false);

                        player.sendMessage(Data.prefix + "Du hast die §cWartungen §7erfolgreich §cDeaktiviert§7.");
                    }else{
                        CloudProxy.getInstance().getProxyGroup().getProxyConfig().setMaintenance(true);

                        player.sendMessage(Data.prefix + "Du hast die §cWartungen §7erfolgreich §aAkiviert§7.");
                    }
                }else{
                    player.sendMessage(Data.prefix + "Versuche /§6Wartungen");
                }
            }else{
                player.sendMessage(Data.noPerm);
            }
        }
    }
}
