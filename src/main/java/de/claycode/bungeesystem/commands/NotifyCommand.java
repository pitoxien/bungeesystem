package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.sql.NotifyStats;
import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class NotifyCommand extends Command {

    public NotifyCommand(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if (player.hasPermission("bungee.notify")) {
                if(args.length == 1) {
                    if(args[0].equalsIgnoreCase("login")) {
                        if (NotifyStats.getNotify(player.getUniqueId().toString()) != true) {
                            NotifyStats.setNotify(player.getUniqueId().toString(), true);

                            player.sendMessage(Data.prefix + "Du bekommst §anun §7alle §aNachrichten§7.");
                        }else if(NotifyStats.getNotify(player.getUniqueId().toString()) == true){
                            player.sendMessage(Data.prefix + "Du bist schon §aeingeloggt§7.");
                        }
                    }else if(args[0].equalsIgnoreCase("logout")) {
                        if(NotifyStats.getNotify(player.getUniqueId().toString()) != false) {
                            NotifyStats.setNotify(player.getUniqueId().toString(), false);

                            player.sendMessage(Data.prefix + "Du bekommst §cnun keine §aNachrichten §7mehr.");
                        }else if(NotifyStats.getNotify(player.getUniqueId().toString()) == false){
                            player.sendMessage(Data.prefix + "Du bist schon §caugeloggt§7.");
                        }
                    }else{
                        player.sendMessage(Data.prefix + "Versuche /§6Notify §8(§aLogin §8| §cLogout§8)");
                    }
                }else{
                    player.sendMessage(Data.prefix + "Versuche /§6Notify §8(§aLogin §8| §cLogout§8)");
                }
            }else{
                player.sendMessage(Data.noPerm);
            }
        }

    }
}
