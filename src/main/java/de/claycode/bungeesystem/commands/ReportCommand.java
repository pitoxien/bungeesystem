package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.sql.NotifyStats;
import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.HashMap;

public class ReportCommand extends Command {

    public ReportCommand(String name) {
        super(name);
    }

    private final HashMap<ProxiedPlayer, ServerInfo> reportMap = new HashMap<>();

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if(args.length == 0) {
                player.sendMessage(Data.prefix + "§8§m------------------------------------");
                player.sendMessage(Data.prefix + "");
                player.sendMessage(Data.prefix + "§7Versuche /§6Report §8(§aSpieler§8) (§cGrund§8)");
                player.sendMessage(Data.prefix + "");
                player.sendMessage(Data.prefix + "§cGründe§8:");
                player.sendMessage(Data.prefix + "§8➥ §cHacking§8, §5Teaming§8, §cChat§8, §dSkin");
                player.sendMessage(Data.prefix + "");
                player.sendMessage(Data.prefix + "§8§m------------------------------------");
            }else if(args.length >= 1) {
                final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);

                if (target != null) {

                    if (target != player) {

                        if (!target.hasPermission("bungee.report.bypass")) {

                            if(args.length == 2) {

                                if(args[1].equalsIgnoreCase("Hacking")) {

                                    reportMap.put(target, target.getServer().getInfo());

                                    for (ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
                                        if(NotifyStats.getNotify(players.getUniqueId().toString()) == true) {
                                            players.sendMessage("§8§m---------------------------");
                                            players.sendMessage("");
                                            players.sendMessage("§7Report von §8➥ §a" + player.getName());
                                            players.sendMessage("§7Reporterter §8➥ §c" + target.getName());
                                            players.sendMessage("§7Grund §8➥ §4Hacking");
                                            players.sendMessage("§7Server §8➥ §e" + target.getServer().getInfo().getName());
                                            players.sendMessage("");

                                            TextComponent textComponent = new TextComponent("§8➥ §7Klicke hier");
                                            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Klicke hier um den §cReport §7zu bearbeiten.").create()));
                                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/Report " + target.getName() + " accept"));

                                            players.sendMessage(textComponent);

                                            players.sendMessage("");
                                            players.sendMessage("§8§m---------------------------");
                                        }
                                    }
                                }else if(args[1].equalsIgnoreCase("Teaming")) {

                                    reportMap.put(target, target.getServer().getInfo());

                                    for (ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
                                        if(NotifyStats.getNotify(players.getUniqueId().toString()) == true) {
                                            players.sendMessage("§8§m---------------------------");
                                            players.sendMessage("");
                                            players.sendMessage("§7Report von §8➥ §a" + player.getName());
                                            players.sendMessage("§7Reporterter §8➥ §c" + target.getName());
                                            players.sendMessage("§7Grund §8➥ §5Teaming");
                                            players.sendMessage("§7Server §8➥ §e" + target.getServer().getInfo().getName());
                                            players.sendMessage("");

                                            TextComponent textComponent = new TextComponent("§8➥ §7Klicke hier");
                                            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Klicke hier um den §cReport §7zu bearbeiten.").create()));
                                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/Report " + target.getName() + " accept"));

                                            players.sendMessage(textComponent);

                                            players.sendMessage("");
                                            players.sendMessage("§8§m---------------------------");
                                        }
                                    }
                                }else if(args[1].equalsIgnoreCase("chat")) {

                                    reportMap.put(target, target.getServer().getInfo());

                                    for (ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
                                        if(NotifyStats.getNotify(players.getUniqueId().toString()) == true) {
                                            players.sendMessage("§8§m---------------------------");
                                            players.sendMessage("");
                                            players.sendMessage("§7Report von §8➥ §a" + player.getName());
                                            players.sendMessage("§7Reporterter §8➥ §c" + target.getName());
                                            players.sendMessage("§7Grund §8➥ §cChat");
                                            players.sendMessage("§7Server §8➥ §e" + target.getServer().getInfo().getName());
                                            players.sendMessage("");

                                            TextComponent textComponent = new TextComponent("§8➥ §7Klicke hier");
                                            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Klicke hier um den §cReport §7zu bearbeiten.").create()));
                                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/Report " + target.getName() + " accept"));

                                            players.sendMessage(textComponent);

                                            players.sendMessage("");
                                            players.sendMessage("§8§m---------------------------");
                                        }
                                    }
                                }else if(args[1].equalsIgnoreCase("skin")) {

                                    reportMap.put(target, target.getServer().getInfo());

                                    for (ProxiedPlayer players : ProxyServer.getInstance().getPlayers()) {
                                        if(NotifyStats.getNotify(players.getUniqueId().toString()) == true) {
                                            players.sendMessage(Data.prefix + "§8§m---------------------------");
                                            players.sendMessage(Data.prefix + "");
                                            players.sendMessage(Data.prefix + "§7Report von §8➥ §a" + player.getName());
                                            players.sendMessage(Data.prefix + "§7Reporterter §8➥ §c" + target.getName());
                                            players.sendMessage(Data.prefix + "§7Grund §8➥ §dSkin");
                                            players.sendMessage(Data.prefix + "§7Server §8➥ §e" + target.getServer().getInfo().getName());
                                            players.sendMessage(Data.prefix + "");

                                            TextComponent textComponent = new TextComponent("§8➥ §7Klicke hier");
                                            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Klicke hier um den §cReport §7zu bearbeiten.").create()));
                                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/Report " + target.getName() + " accept"));

                                            players.sendMessage(textComponent);

                                            players.sendMessage(Data.prefix + "");
                                            players.sendMessage(Data.prefix + "§8§m---------------------------");
                                        }
                                    }
                                }else if (args[1].equalsIgnoreCase("accept")) {
                                    if(reportMap.containsKey(target)) {
                                        player.connect(reportMap.get(target));

                                        player.sendMessage(Data.prefix + "Du §cbearbeitest §7den §cReport §7von §c" + target.getName() + "§7.");
                                        reportMap.remove(target);
                                    }else{
                                        player.sendMessage(Data.prefix + "Der Spieler wurde §cnicht §7Reportet.");
                                    }
                                }
                            }else{
                                player.sendMessage(Data.prefix + "§7Versuche /§6Report §8(§aSpieler§8) (§cGrund§8)");
                            }

                        }else{
                            player.sendMessage(Data.prefix + "Diesen Spieler kannst du §cnicht §7Reporten.");
                        }

                    }else{
                        player.sendMessage(Data.prefix + "Du darfst dich §cnicht §7selber §cReporten§7.");
                    }

                }else{
                    player.sendMessage(Data.prefix + "Der Spieler ist §cnicht §7Online.");
                }
            }
        }
    }
}
