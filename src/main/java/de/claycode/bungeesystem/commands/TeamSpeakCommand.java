package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.BungeeSystem;
import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class TeamSpeakCommand extends Command {

    public TeamSpeakCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if(args.length == 0){
                player.sendMessage(Data.prefix + "Du kannst unseren §bTeamSpeak §7unter§8: §6Pitoxien.net");
            }else{
                player.sendMessage(Data.prefix + "Versuche /§6TeamSpeak");
            }
        }
    }
}
