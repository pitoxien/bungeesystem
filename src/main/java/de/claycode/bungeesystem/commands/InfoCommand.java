package de.claycode.bungeesystem.commands;

import de.claycode.bungeesystem.storage.Data;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class InfoCommand extends Command {

    public InfoCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxyServer.getInstance().getConsole().sendMessage(Data.prefix + "Du bist §ckein §7Spieler.");
        }else{
            final ProxiedPlayer player = (ProxiedPlayer) sender;

            if(player.hasPermission("bungee.info")) {
                if(args.length == 1) {
                    final ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);

                    if(target != null) {
                        player.sendMessage("§8§m------------------------------");
                        player.sendMessage("");
                        player.sendMessage("§7Name §8➥ §a" + target.getName());
                        player.sendMessage("§7IP §8➥ §c" + target.getAddress().getAddress().toString().replace("/", "") + "§8:§c" + target.getAddress().getPort());
                        player.sendMessage("§7Server §8➥ §a" + target.getServer().getInfo().getName());
                        player.sendMessage("");
                        player.sendMessage("§8§m------------------------------");
                    }else{
                        player.sendMessage(Data.prefix + "Der Spieler ist §cnicht §7Online.");
                    }
                }else{
                    player.sendMessage(Data.prefix + "Versuche /§6Info §8(§aSpieler§8)");
                }
            }else{
                player.sendMessage(Data.noPerm);
            }
        }
    }
}
